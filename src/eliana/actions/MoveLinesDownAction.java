/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

import jsyntaxpane.SyntaxDocument;

/**
 *
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class MoveLinesDownAction extends SyntaxAction {

	private static final Logger LOGGER = Logger.getLogger(
			MoveLinesDownAction.class.getName());

	public MoveLinesDownAction() {
		super("Move lines down", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,
				InputEvent.ALT_DOWN_MASK));
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		JTextComponent textComponent = getTextComponent(e);
		if (textComponent != null) {
			SyntaxDocument doc = (SyntaxDocument) textComponent.getDocument();
			try {
				moveLinesDown(doc, textComponent);
			} catch (BadLocationException ex) {
				LOGGER.log(Level.SEVERE, null, ex);
			}
		}
	}

	private void moveLinesDown(SyntaxDocument document,
			JTextComponent textComponent) throws BadLocationException {

		int selStart =  textComponent.getSelectionStart();

		int start = document.getLineStartOffset(selStart);
		int secondLineStart = document.getLineEndOffset(selStart) + 1;

		int end = document.getLineEndOffset(textComponent.getSelectionEnd());


		String dupLines = document.getText(start, end - start);

		document.insertString(start, dupLines, null);
	}

}

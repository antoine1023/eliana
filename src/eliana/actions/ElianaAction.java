/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.text.TextAction;

/**
 * All Eliana keyboard related actions implement this class.
 *
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public abstract class ElianaAction extends TextAction {

	/**
	 * @param name The action's displayed name.
	 * @param keyStroke The action's key stroke.
	 */
	public ElianaAction(String name, KeyStroke keyStroke) {
		super(name);
		putValue(ACCELERATOR_KEY, keyStroke);
	}

	public KeyStroke getKeyStroke() {
		return (KeyStroke) getValue(ACCELERATOR_KEY);
	}

	public void setKeyStroke(KeyStroke keyStroke) {
		putValue(ACCELERATOR_KEY, keyStroke);
	}

	public String getName() {
		return (String) getValue(NAME);
	}

	public void setName(String name) {
		putValue(NAME, name);
	}

	public void setIcon(Icon icon) {
		putValue(Action.SMALL_ICON, icon);
	}

}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.JTextComponent;

/**
 * A simple dialog to prompt for a line number and go to it
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class GotoLineDialog extends JDialog {

	private static final Logger LOGGER = Logger.getLogger(
			GotoLineDialog.class.getName());

	private final JButton okButton;
	private final JTextField textField;
	private final JLabel errorLabel;

	private JTextComponent textComponent;

	/**
	 * Creates new form GotoLineDialog
	 * @param textComponent
	 */
	public GotoLineDialog(JTextComponent textComponent,
			String dialogTitle,
			String dialogText,
			String okButtonText,
			String cancelButtonText,
			String invalidNumberString) {

		super(ActionUtils.getFrameFor(textComponent), true);

		setLocationRelativeTo(textComponent.getRootPane());
		setSize(350, 160);
		setTitle(dialogTitle);

		this.textComponent = textComponent;

		{
			okButton = new JButton();
			okButton.setText(okButtonText);
			okButton.setEnabled(true);
			getRootPane().setDefaultButton(okButton);
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent evt) {
					okButtonClicked(evt);
				}
			});

			JPanel buttonPanel = new JPanel(
					new FlowLayout(FlowLayout.RIGHT, 10, 10));
			buttonPanel.add(okButton);
			add(buttonPanel, BorderLayout.SOUTH);
		}

		{
			textField = new JTextField(10);
			textField.setText("0");
			textField.addCaretListener(new CaretListener() {
				@Override
				public void caretUpdate(CaretEvent arg0) {
					textFieldCaretUpdate();
				}
			});

			errorLabel = new JLabel(invalidNumberString);
			errorLabel.setVisible(false);

			Box box = Box.createVerticalBox();
			box.add(new JLabel(dialogText));
			box.add(Box.createVerticalStrut(10));
			box.add(textField);
			box.add(Box.createVerticalStrut(10));
			box.add(errorLabel);

			JPanel buttonPanel = new JPanel(
					new FlowLayout(FlowLayout.LEFT, 10, 10));
			buttonPanel.add(box);
			add(buttonPanel, BorderLayout.CENTER);
		}

	}

	private void okButtonClicked(ActionEvent evt) {
		goToLine();
	}

	private void textFieldCaretUpdate() {
		Integer lineNumber = parseLineNumber();
		if (lineNumber == null) {
			okButton.setEnabled(false);
			errorLabel.setVisible(true);
		} else {
			okButton.setEnabled(true);
			errorLabel.setVisible(false);
		}
	}

	private Integer parseLineNumber() {
		String text = textField.getText().trim();
		try {
			return Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private void goToLine() {
		Integer lineNumber = parseLineNumber();
		if (lineNumber == null) {
			LOGGER.warning("Cannot parse " + textField.getText());
		} else {
			int pos = ActionUtils.getDocumentPosition(textComponent,
					lineNumber, 0);
			textComponent.setCaretPosition(pos);
		}
		setVisible(false);
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible == true) {
			textField.requestFocusInWindow();
			textField.selectAll();
		}

		super.setVisible(visible);
	}

}

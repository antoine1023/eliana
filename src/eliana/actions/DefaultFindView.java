/*
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.Frame;
import java.util.regex.Pattern;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.text.JTextComponent;


/**
 * TODO: Improve
 *
 */
public class DefaultFindView implements FindView {

	private final DefaultFindDialog findReplaceDialog;

	public DefaultFindView(
			Frame parent,
			Action replaceAction,
			Action replaceAllAction,
			Action findNextAction) {

		findReplaceDialog = new DefaultFindDialog(
				parent,
				replaceAction,
				replaceAllAction,
				findNextAction);
	}


	@Override
	public void notFound(JTextComponent textComponent) {
		JOptionPane.showMessageDialog(textComponent.getRootPane(),
				"Not found");
	}

	@Override
	public void showFindView(JTextComponent textComponent) {
		JOptionPane.showMessageDialog(textComponent.getRootPane(),
				"Search");
	}

	@Override
	public void showFindReplaceView(JTextComponent textComponent) {
		findReplaceDialog.setVisible(true);
	}

	@Override
	public Pattern getPattern() {
		return Pattern.compile(findReplaceDialog.getTarget(), Pattern.LITERAL);
	}

}

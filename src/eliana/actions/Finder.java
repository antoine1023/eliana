/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eliana.actions;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.JTextComponent;

import eliana.ElianaDocument;
import eliana.components.Markers;


/**
 * Searches and replaces text
 */
public class Finder {

	private static Markers.SimpleMarker SEARCH_MARKER =
			new Markers.SimpleMarker(Color.YELLOW);

	private FindView view;
	private JTextComponent textComponent;



	public Finder() {
	}


	public void setView(FindView view) {
		this.view = view;
	}


	/**
	 * Shows the find / replace view for this component.
	 * @param textComponent
	 */
	public void showFindReplaceView(JTextComponent textComponent) {
		this.textComponent = textComponent;
		view.showFindReplaceView(textComponent);
	}


	/**
	 * Performs a "find next" operation on the given text component.  Positions
	 * the caret at the start of the next found pattern
	 */
	public void findNext() {
		ElianaDocument document = ActionUtils.getDocument(textComponent);

		int start = textComponent.getCaretPosition() + 1;

		// we must advance the position by one, otherwise we will find
		// the same text again
		if (start >= document.getLength()) {
			start = 0;
		}

		Matcher matcher = document.getMatcher(getPattern(), start);

		if (matcher != null && matcher.find()) {

			// since we used an offset in the matcher, the matcher location
			// MUST be offset by that location
			textComponent.select(matcher.start() + start,
					matcher.end() + start);
			System.out.println("FindReplaceAction.findNext()");

		} else {

			matcher = document.getMatcher(getPattern());
			if (matcher != null && matcher.find()) {
				textComponent.select(matcher.start(), matcher.end());
			} else {
				view.notFound(textComponent);
			}

		}
	}


	/**
	 * Performs a replace all operation on the given component.<br>
	 * Note that this creates a new duplicate String big as the entire
	 * document and then assigns it to the target text component
	 * 
	 */
	public void replaceAll(String replacement) {
		ElianaDocument document = ActionUtils.getDocument(textComponent);

		Matcher matcher = document.getMatcher(getPattern());
		String newText = matcher.replaceAll(replacement);
		textComponent.setText(newText);
	}


	private Pattern getPattern() {
		return view.getPattern();
	}


	/**
	 * Updates the highlights in the document.
	 */
	public void updateHighlights(Pattern pattern) {
		Markers.removeMarkers(textComponent, SEARCH_MARKER);
		Markers.markAll(textComponent, pattern, SEARCH_MARKER);
	}

}

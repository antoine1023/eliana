/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;

import eliana.ElianaDocument;

/**
 * IndentAction is used to replace Tabs with spaces.  If there is selected
 * text, then the lines spanning the selection will be shifted
 * right by one tab-width space  character
 */
@SuppressWarnings("serial")
public class IndentAction extends ElianaAction {

	public IndentAction() {
		super("Indent", KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextComponent target = getTextComponent(e);
		if (target != null) {

			ElianaDocument document = (ElianaDocument) target.getDocument();

			String selected = target.getSelectedText();

			if (selected == null) {

				if (document.getIndentWithTabs()) {
					target.replaceSelection("\t");
				} else {
					insertSpaceIndent(target, document);
				}

			} else {

				String indent = document.getIndentWithTabs() ?
						"\t" : document.getSpaceIndentString();

				String[] lines = ActionUtils.getSelectedLines(target);
				int start = target.getSelectionStart();
				StringBuilder sb = new StringBuilder();
				for (String line : lines) {
					sb.append(indent);
					sb.append(line);
					sb.append('\n');
				}
				target.replaceSelection(sb.toString());
				target.select(start, start + sb.length());
			}
		}
	}

	private void insertSpaceIndent(JTextComponent textComponent,
			ElianaDocument document) {

		int tabStop = document.getTabSize();

		Element line = document.getParagraphElement(
				textComponent.getCaretPosition());

		int lineStart = line.getStartOffset();
		int column = textComponent.getCaretPosition() - lineStart;
		int needed = tabStop - (column % tabStop);
		textComponent.replaceSelection(
				ActionUtils.repeatString(" ", needed));
	}

}

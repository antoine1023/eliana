/*
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eliana.actions;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * A simple find/replace dialog.
 *
 */
@SuppressWarnings("serial")
public class DefaultFindDialog extends JDialog {

	private final JLabel findLabel;
	private final JTextField findTextField;

	private final JLabel replaceWithLabel;
	private final JTextField replaceTextField;
	private final JCheckBox caseCheckBox;

	private final JButton closeButton;



	public DefaultFindDialog(Frame parent,
			Action replaceAction,
			Action replaceAllAction,
			Action findNextAction) {

		this(parent,
				new JButton(replaceAction),
				new JButton(replaceAllAction),
				new JButton(findNextAction));
	}


	public DefaultFindDialog(Frame parent,
			JButton replaceButton,
			JButton replaceAllButton,
			JButton findNextButton) {

		super(parent, false);

		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setTitle("Find / Replace");
		setBounds(100, 100, 501, 196);
		getContentPane().setLayout(new BorderLayout());

		{
			Box verticalBox = Box.createVerticalBox();
			verticalBox.setBorder(BorderFactory.createEmptyBorder(
					10, 10, 10, 10));
			getContentPane().add(verticalBox, BorderLayout.NORTH);

			{
				Box findHBox = Box.createHorizontalBox();
				verticalBox.add(findHBox);

				findLabel = new JLabel("Find");
				findHBox.add(findLabel);

				findHBox.add(Box.createHorizontalStrut(20));

				findTextField = new JTextField();
				findHBox.add(findTextField);

			}

			{
				Component verticalStrut = Box.createVerticalStrut(10);
				verticalBox.add(verticalStrut);
			}

			{
				Box hBox = Box.createHorizontalBox();
				verticalBox.add(hBox);

				replaceWithLabel = new JLabel("Replace with");
				hBox.add(replaceWithLabel);

				Component horizontalStrut = Box.createHorizontalStrut(10);
				hBox.add(horizontalStrut);

				replaceTextField = new JTextField();
				hBox.add(replaceTextField);
			}

			verticalBox.add(Box.createVerticalStrut(20));

			{
				Box horizontalBox = Box.createHorizontalBox();

				caseCheckBox = new JCheckBox("Match case");
				caseCheckBox.setVerticalAlignment(SwingConstants.TOP);
				caseCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
				horizontalBox.add(caseCheckBox);
				horizontalBox.add(Box.createHorizontalGlue());

				verticalBox.add(horizontalBox);
			}

		}

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);

			closeButton = new JButton("Close");
			buttonPane.add(closeButton);
			closeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}
			});
			getRootPane().setDefaultButton(closeButton);


			buttonPane.add(replaceAllButton);


			buttonPane.add(replaceButton);


			buttonPane.add(findNextButton);

		}

	}

	public void setFindLabel(String s) {
		findLabel.setText(s);
	}

	public void setCloseButton(String s) {
		closeButton.setText(s);
	}

	public void setMatchCaseCheckBox(String s) {
		caseCheckBox.setText(s);
	}

	public String getTarget() {
		return findTextField.getText();
	}

	public String getReplacement() {
		return replaceTextField.getText();
	}

}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.event.ActionEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;

import jsyntaxpane.SyntaxDocument;
import jsyntaxpane.util.Configuration;

/**
 * Finder class.  This class contains the general Find, Find Next,
 * Find Previous, and the Find Marker Actions.
 * 
 * Note that all Actions are subclasses of this class because all actions
 * require the find text to be shared among them.  This is the best approach
 * to have all Action classes share this same data.
 *
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class FindReplaceActions extends SyntaxAction {

	private Pattern pattern = null;
	private boolean wrap = true;
	private final FindDialogAction findDialogAction = new FindDialogAction();
	private final FindNextAction findNextAction = new FindNextAction();
	private ReplaceDialog dlg;

	public FindReplaceActions(String name, KeyStroke keyStroke) {
		super(name, keyStroke);
	}

	public TextAction getFindDialogAction() {
		return findDialogAction;
	}

	public TextAction getFindNextAction() {
		return findNextAction;
	}

//	@Override
//	public TextAction getAction(String key) {
//		if(key.equals("FIND") ) {
//			return findDialogAction;
//		} else if(key.equals("REPLACE")) {
//			return findDialogAction;
//		} else if(key.equals("FIND_NEXT")) {
//			return findNextAction;
//		} else {
//			throw new IllegalArgumentException("Bad Action: " + key);
//		}
//	}

	/**
	 * This class displays the Find Dialog.  The dialog will use the pattern
	 * and will update it once it is closed.
	 */
	@SuppressWarnings("serial")
	class FindDialogAction extends TextAction {

		public FindDialogAction() {
			super("FIND_ACTION");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextComponent target = getTextComponent(e);
			if (target != null) {
				showDialog(target);
			}
		}
	}

	/**
	 * This class performs a Find Next operation by using the current pattern
	 */
	@SuppressWarnings("serial")
	class FindNextAction extends TextAction {

		public FindNextAction() {
			super("FIND_NEXT");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// if we did not start searching, return now
			if (pattern == null) {
				return;
			}
			JTextComponent target = getTextComponent(e);
			doFindNext(target);
		}
	}

	/**
	 * Display an OptionPane dialog that the search string is not found
	 */
	public void msgNotFound() {
		JOptionPane.showMessageDialog(null,
				"Search String " + pattern + " not found",
				"Find", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Show the dialog
	 * @param targetFrame
	 * @param sDoc
	 * @param target
	 */
	private void showDialog(JTextComponent target) {
		if (dlg == null) {
			dlg = new ReplaceDialog(target, FindReplaceActions.this);
		}
		dlg.setVisible(true);
	}


	// - Getters and setters -------------------------------------------------
	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public boolean isWrap() {
		return wrap;
	}

	public void setWrap(boolean wrap) {
		this.wrap = wrap;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	}
}
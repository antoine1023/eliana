/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.event.ActionEvent;
import java.util.WeakHashMap;

import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

/**
 * This actions displays the GotoLine dialog
 */
@SuppressWarnings("serial")
public class GoToLineAction extends ElianaAction {

	private static final WeakHashMap<JTextComponent, GotoLineDialog> DIALOGS =
			new WeakHashMap<JTextComponent, GotoLineDialog>();

	private String dialogTitle = "Go to line";
	private String dialogText = "Enter line number:";
	private String okButtonText = "OK";
	private String cancelButtonText = "Cancel";
	private String invalidNumberMessage = "Invalid number";

	public GoToLineAction() {
		super("Go to line ...", KeyStroke.getKeyStroke("control L"));
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		JTextComponent textComponent = getTextComponent(e);
		GotoLineDialog dialog = DIALOGS.get(textComponent);
		if(dialog == null) {
			dialog = new GotoLineDialog(textComponent,
					dialogTitle,
					dialogText,
					okButtonText,
					cancelButtonText,
					invalidNumberMessage);
			DIALOGS.put(textComponent, dialog);
		}
		dialog.setVisible(true);
	}

}

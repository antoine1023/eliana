/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.actions;

import java.awt.event.ActionEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

import eliana.ElianaDocument;

/**
 * This action will toggle comments on or off on selected whole lines.
 * 
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class ToggleCommentsAction extends ElianaAction {

	protected String lineCommentStart = "// ";
	protected Pattern lineCommentPattern =
			Pattern.compile("(^" + lineCommentStart + ")(.*)");


	/**
	 * creates new ToggleCommentsAction.
	 * Initial Code contributed by ser... AT mail.ru
	 */
	public ToggleCommentsAction(String name, KeyStroke keyStroke) {
		super(name, keyStroke);
	}

	/**
	 * {@inheritDoc}
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JTextComponent target = getTextComponent(e);
		if (target != null && target.getDocument() instanceof ElianaDocument) {
			String[] lines = ActionUtils.getSelectedLines(target);
			StringBuffer toggled = new StringBuffer();
			for (String line : lines) {
				Matcher m = lineCommentPattern.matcher(line);
				if (m.find()) {
					toggled.append(m.replaceFirst("$2"));
				} else {
					toggled.append(lineCommentStart);
					toggled.append(line);
				}
				toggled.append('\n');
			}
			target.replaceSelection(toggled.toString());
		}
	}
}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.awt.Graphics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.Segment;
import javax.swing.text.TabExpander;

/**
 * The styles to use for each {@link ElianaTokenType}.
 * 
 * @author Ayman
 */
public class Theme {

	private final Map<ElianaTokenType, TextStyle> styles;

	public TextStyle defaultStyle = TextStyle.DEFAULT_STYLE;


	public Theme() {
		styles = new HashMap<ElianaTokenType, TextStyle>();
	}



	/**
	 * Adds a new {@link TextStyle}.
	 * @param type
	 * @param style
	 */
	public void put(ElianaTokenType type, TextStyle style) {
		styles.put(type, style);
	}

	public void put(List<ElianaTokenType> types, TextStyle style) {
		for (ElianaTokenType type : types) {
			styles.put(type, style);
		}
	}

	public void put(ElianaTokenType[] types, TextStyle style) {
		for (ElianaTokenType type : types) {
			styles.put(type, style);
		}
	}

	/**
	 * Merge m into this.
	 * @param m
	 */
	public void putAll(Map<? extends ElianaTokenType, ? extends TextStyle> m) {
		styles.putAll(m);
	}

	/**
	 * Merge theme into this.
	 * @param theme
	 */
	public void putAll(Theme theme) {
		styles.putAll(theme.styles);
	}

	/**
	 * @param type
	 * @return The style for the given {@link ElianaTokenType}
	 */
	public TextStyle getStyle(ElianaTokenType type) {
		if (styles.containsKey(type)) {
			return styles.get(type);
		} else {
			return defaultStyle;
		}
	}

	public void setDefaultStyle(TextStyle style) {
		defaultStyle = style;
	}

	/**
	 * Draw the given {@link Segment}.
	 * 
	 * This will simply find the proper {@link TextStyle} for the
	 * {@link ElianaTokenType} and then asks the proper Style to draw
	 * the text of the {@link ElianaToken}.
	 * 
	 * @param segment
	 * @param x
	 * @param y
	 * @param graphics
	 * @param tabExpander
	 * @param token
	 * @return the X location at the end of the rendered text
	 */
	public int drawText(Segment segment, int x, int y,
			Graphics graphics, TabExpander tabExpander, ElianaToken token) {

		TextStyle s = getStyle(token.getType());
		return s.drawText(segment, x, y, graphics, tabExpander,
				token.getStart());
	}
}

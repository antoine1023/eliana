/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.components;

import java.awt.Color;

import javax.swing.JEditorPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.JTextComponent;

import eliana.ElianaDocument;
import eliana.ElianaToken;
import eliana.actions.*;

/**
 * This class highlights any pairs of the given language.  Pairs are defined
 * with the Token.pairValue.
 *
 * @author Ayman Al-Sairafi
 */
public class PairsMarker implements CaretListener, ElianaComponent {

	private JTextComponent pane;
	private final Markers.SimpleMarker marker;



	public PairsMarker() {
		this(new Color(0xeeee33));
	}

	public PairsMarker(Color color) {
		this.marker = new Markers.SimpleMarker(color);
	}



	@Override
	public void caretUpdate(CaretEvent e) {
		removeMarkers();
		int pos = e.getDot();
		ElianaDocument doc = ActionUtils.getDocument(pane);
		ElianaToken token = doc.getTokenAt(pos);
		if (token != null && token.getPairValue() != 0) {
			Markers.markToken(pane, token, marker);
			ElianaToken other = doc.getPairFor(token);
			if (other != null) {
				Markers.markToken(pane, other, marker);
			}
		}
	}

	/**
	 * Remove all the highlights from the editor pane.  This should be called
	 * when the editorkit is removed.
	 */
	public void removeMarkers() {
		Markers.removeMarkers(pane, marker);
	}

	@Override
	public void install(JEditorPane editor) {
		pane = editor;
		pane.addCaretListener(this);
	}

	@Override
	public void deinstall(JEditorPane editor) {
		pane.removeCaretListener(this);
		removeMarkers();
	}
}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.components;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import eliana.ElianaDocument;
import eliana.actions.ActionUtils;

/**
 * LineRuleis used to number the lines in the EdiorPane
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class LineNumbersRuler extends JComponent
implements ElianaComponent, PropertyChangeListener, DocumentListener {

	private JEditorPane editorPane;
	private String format;

	private int lineCount = -1;
	private int rightMargin = 5;
	private int leftMargin = 5;
	private int minimumWidth = 30;

	private int charHeight;
	private int charWidth;

	private Action goToLineAction;
	private MouseListener mouseListener;

	/**
	 * The status is used to have proper propertyChange support.
	 * We need to know if we are INSTALLING the component or DE-INSTALLING it
	 */
	static enum Status {
		INSTALLING,
		DEINSTALLING
	}

	private Status status;



	public LineNumbersRuler() {
		setBackground(new Color(0xdddddd));
	}

	
	public void setGoToLineAction(Action goToLineAction) {
		this.goToLineAction = goToLineAction;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D graphics2D = (Graphics2D) g;
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setFont(editorPane.getFont());
		Rectangle clip = g.getClipBounds();
		g.setColor(getBackground());
		g.fillRect(clip.x, clip.y, clip.width, clip.height);
		g.setColor(getForeground());
		int lh = charHeight;
		int end = clip.y + clip.height + lh;
		int lineNum = clip.y / lh + 1;
		// round the start to a multiple of lh, and shift by 2 pixels to align
		// properly to the text.
		for (int y = (clip.y / lh) * lh + lh - 2; y <= end; y += lh) {
			String text = String.format(format, lineNum);
			g.drawString(text, leftMargin, y);
			lineNum++;
			if (lineNum > lineCount) {
				break;
			}
		}
	}

	/**
	 * Update the size of the line numbers based on the length of the document
	 */
	private void updateSize() {
		int newLineCount = ActionUtils.getLineCount(editorPane);
		if (newLineCount == lineCount) {
			return;
		}
		lineCount = newLineCount;
		int h = lineCount * charHeight + editorPane.getHeight();
		int d = (int) Math.log10(lineCount) + 1;
		if (d < 1) {
			d = 1;
		}

		int w = d * charWidth + rightMargin + leftMargin;
		w = Math.max(w, minimumWidth);

		format = "%" + d + "d";
		setPreferredSize(new Dimension(w, h));
		if(getParent() != null){
			getParent().doLayout();
		}
	}

	/**
	 * @param editorPane
	 * @return the JscrollPane that contains this EditorPane, or
	 * <code>null</code> if no JScrollPane is the parent of this editor
	 */
	private JScrollPane getScrollPane(JTextComponent editorPane) {
		Container p = editorPane.getParent();
		while (p != null) {
			if (p instanceof JScrollPane) {
				return (JScrollPane) p;
			}
			p = p.getParent();
		}
		return null;
	}


	@Override
	public void install(JEditorPane editor) {
		this.editorPane = editor;
		charHeight = editorPane.getFontMetrics(editorPane.getFont()).getHeight();
		charWidth = editorPane.getFontMetrics(editorPane.getFont()).charWidth('0');
		editor.addPropertyChangeListener(this);
		JScrollPane sp = getScrollPane(editorPane);
		if (sp == null) {
			Logger.getLogger(this.getClass().getName()).warning(
					"JEditorPane is not enclosed in JScrollPane, " +
					"no LineNumbers will be displayed");
		} else {
			sp.setRowHeaderView(this);
			this.editorPane.getDocument().addDocumentListener(this);
			updateSize();
			mouseListener = new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (goToLineAction == null)
						return;
					ActionEvent ae = new ActionEvent(e.getSource(), e.getID(),
							null);
					goToLineAction.actionPerformed(ae);
				}
			};
			addMouseListener(mouseListener);
		}
		status = Status.INSTALLING;
	}

	@Override
	public void deinstall(JEditorPane editor) {
		removeMouseListener(mouseListener);
		status = Status.DEINSTALLING;
		JScrollPane sp = getScrollPane(editor);
		if (sp != null) {
			editor.getDocument().removeDocumentListener(this);
			sp.setRowHeaderView(null);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("document")) {
			if (evt.getOldValue() instanceof ElianaDocument) {
				ElianaDocument syntaxDocument = (ElianaDocument) evt.getOldValue();
				syntaxDocument.removeDocumentListener(this);
			}
			if (evt.getNewValue() instanceof ElianaDocument && status.equals(Status.INSTALLING)) {
				ElianaDocument syntaxDocument = (ElianaDocument) evt.getNewValue();
				syntaxDocument.addDocumentListener(this);
			}
		} else if (evt.getPropertyName().equals("font")) {
			charHeight = editorPane.getFontMetrics(editorPane.getFont()).getHeight();
			charWidth = editorPane.getFontMetrics(editorPane.getFont()).charWidth('0');
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		updateSize();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		updateSize();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		updateSize();
	}
}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.Timer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import eliana.ElianaDocument;
import eliana.ElianaToken;
import eliana.actions.ActionUtils;

/**
 * This class highlights Tokens within a document whenever the caret is moved
 * to a TokenType provided in the config file.
 * 
 * @author Ayman Al-Sairafi
 */
public class TokenMarker implements ElianaComponent, CaretListener {

	private static final Logger LOGGER = Logger.getLogger(
			TokenMarker.class.getName());

	private static final int MAX_TRAVERSABLE_TOKEN_COUNT = 4000;

	private final Timer timer;

	private JEditorPane editorPane;
	private Markers.SimpleMarker marker;


	/**
	 * Constructs a new {@link TokenMarker}
	 */
	public TokenMarker() {
		this(new Color(0xdddddd));
	}

	/**
	 * Constructs a new {@link TokenMarker}
	 * 
	 * @param color the color for highlighting
	 */
	public TokenMarker(Color color) {
		marker = new Markers.SimpleMarker(color);

		ActionListener updateMarkersListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateMarkers();
			}
		};

		timer = new Timer(500, updateMarkersListener);
		timer.setRepeats(false);
	}



	private void updateMarkers() {
		removeMarkers();
		ElianaDocument doc = ActionUtils.getDocument(editorPane);
		int pos = editorPane.getCaretPosition();
		ElianaToken token = doc.getTokenAt(pos);
		if (token != null && token.getType().areOccurencesMarkable()) {
			try {
				addMarkers(token);
			} catch (BadLocationException e) {
				LOGGER.severe(e.toString());
			}
		}
	}


	@Override
	public void caretUpdate(CaretEvent e) {
		timer.start();
	}

	/**
	 * removes all markers from the pane.
	 */
	public void removeMarkers() {
		Markers.removeMarkers(editorPane, marker);
	}

	/**
	 * add highlights for the given pattern
	 * @param patternToken
	 * @throws BadLocationException
	 */
	private void addMarkers(ElianaToken patternToken)
			throws BadLocationException {

		ElianaDocument document = (ElianaDocument) editorPane.getDocument();

		document.readLock();

		String text = getText(patternToken, document);

		List<ElianaToken> tokens = document.getTokens();

		final int index = document.indexOf(patternToken);
		if (index < 0) {
			// The token has been removed.
			return;
		}

		final int startIndex = Math.max(
				index - MAX_TRAVERSABLE_TOKEN_COUNT, 0);

		final int endIndex = Math.min(
				index + MAX_TRAVERSABLE_TOKEN_COUNT, tokens.size());

		for (int i = startIndex; i < endIndex; i++) {
			ElianaToken token = tokens.get(i);
			if (token.getLength() == patternToken.getLength()
					&& text.equals(getText(token, document))) {
				Markers.markToken(editorPane, token, marker);
			}
		}

		document.readUnlock();
	}

	// TODO: Move somewhere
	private static String getText(ElianaToken token, Document document)
			throws BadLocationException {
		return document.getText(token.getStart(), token.getLength());
	}

	@Override
	public void install(JEditorPane editor) {
		editorPane = editor;
		editorPane.addCaretListener(this);
	}

	@Override
	public void deinstall(JEditorPane editor) {
		removeMarkers();
		editorPane.removeCaretListener(this);
	}

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(
			TokenMarker.class.getName());
}

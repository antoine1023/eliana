/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.PlainView;
import javax.swing.text.Segment;
import javax.swing.text.ViewFactory;

public class ElianaTextView extends PlainView {

	private static final Logger LOGGER = Logger.getLogger(
			ElianaTextView.class.getName());



	private final boolean singleColorSelect = true;

	private final Theme styles;

	/**
	 * 0 if the right margin is disabled.
	 */
	private int rightMarginColumn = 79;

	private int rightMarginWidth = 2;

	private Color rightMarginColor = new Color(0x999999);


	/**
	 * Construct a new view using the given configuration and prefix given
	 * 
	 * @param element
	 * @param styles
	 */
	public ElianaTextView(Element element, Theme styles) {
		super(element);
		this.styles = styles;
	}



	@Override
	protected int drawUnselectedText(Graphics graphics, int x, int y, int p0,
			int p1) {

		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		TextStyle defaultStyle = styles.defaultStyle;

		Font savedFont = graphics.getFont();
		Color savedColor = graphics.getColor();
		ElianaDocument doc = (ElianaDocument) getDocument();
		Segment segment = getLineBuffer();

		// Draw the right margin first, if needed.  This way the text overlays
		// the margin
		if (rightMarginColumn > 0) {

			int mx = rightMarginColumn * graphics.getFontMetrics().charWidth('m');

			int h = graphics.getFontMetrics().getHeight();
			graphics.setColor(rightMarginColor);
			graphics.fillRect(mx, y - h, rightMarginWidth, h);

		}

		try {
			// Colour the parts
			Iterator<ElianaToken> iterator = doc.getTokenIterator(p0, p1);
			int start = p0;

			while (iterator.hasNext()) {

				ElianaToken token = iterator.next();

				// if there is a gap between the next token start and where we
				// should be starting (spaces not returned in tokens), then draw
				// it in the default type
				if (start < token.getStart()) {
					doc.getText(start, token.getStart() - start, segment);
					x = defaultStyle.drawText(segment, x, y, graphics,
							this, start);
				}
				// token and s are the actual start and length of what we should
				// put on the screen.  assume these are the whole token....
				int l = token.getLength();
				int s = token.getStart();
				// ... unless the token starts before p0:
				if (s < p0) {
					// token is before what is requested. adgust the length and s
					l -= (p0 - s);
					s = p0;
				}
				// if token end (s + l is still the token end pos) is greater
				// than p1, then just put up to p1
				if (s + l > p1) {
					l = p1 - s;
				}
				doc.getText(s, l, segment);
				x = styles.drawText(segment, x, y, graphics, this, token);
				start = token.getEnd();
			}
			// now for any remaining text not tokenized:
			if (start < p1) {
				doc.getText(start, p1 - start, segment);
				x = defaultStyle.drawText(segment, x, y, graphics, this, start);
			}
		} catch (BadLocationException ex) {
			LOGGER.severe("Requested: " + ex.offsetRequested());
			LOGGER.log(Level.SEVERE, null, ex);
		} finally {
			graphics.setFont(savedFont);
			graphics.setColor(savedColor);
		}
		return x;
	}

	@Override
	protected int drawSelectedText(Graphics graphics, int x, int y, int p0, int p1)
			throws BadLocationException {
		if (singleColorSelect) {
			if (rightMarginColumn > 0) {
				int m_x = rightMarginColumn * graphics.getFontMetrics().charWidth('m');
				int h = graphics.getFontMetrics().getHeight();
				graphics.setColor(rightMarginColor);
				graphics.drawLine(m_x, y, m_x, y - h);
			}
			return super.drawUnselectedText(graphics, x, y, p0, p1);
		} else {
			return drawUnselectedText(graphics, x, y, p0, p1);
		}
	}

	@Override
	protected void updateDamage(javax.swing.event.DocumentEvent changes,
			Shape a,
			ViewFactory f) {
		super.updateDamage(changes, a, f);
		java.awt.Component host = getContainer();
		host.repaint();
	}
}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import eliana.actions.ElianaAction;
import eliana.components.ElianaComponent;

/**
 * The DefaultSyntaxKit is the main entry to SyntaxPane.  To use the package,
 * just set the EditorKit of the EditorPane to a new instance of this class.
 * 
 * You need to pass a proper lexer to the class.
 * 
 * @author ayman
 */
@SuppressWarnings("serial")
public class ElianaEditorKit extends DefaultEditorKit
implements ViewFactory {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(
			ElianaEditorKit.class.getName());

	private final ElianaLexer lexer;

	private final List<ElianaComponent> components =
			new ArrayList<ElianaComponent>();

	private final List<ElianaAction> actions =
			new ArrayList<ElianaAction>();

	private final Theme styles;



	/**
	 * Create a new Kit for the given language
	 */
	public ElianaEditorKit(ElianaLexer lexer, Theme styles) {

		this.lexer = lexer;
		this.styles = styles;
	}



	public void addAction(ElianaAction action) {
		actions.add(action);
	}

	public void addComponent(ElianaComponent component) {
		components.add(component);
	}

	@Override
	public ViewFactory getViewFactory() {
		return this;
	}

	@Override
	public View create(Element element) {
		return new ElianaTextView(element, styles);
	}

	/**
	 * Install the View on the given EditorPane.  This is called by Swing and
	 * can be used to do anything you need on the JEditorPane control.  Here
	 * I set some default Actions.
	 * 
	 * @param editorPane
	 */
	@Override
	public void install(JEditorPane editorPane) {
		super.install(editorPane);

		Keymap parentKeymap = JTextComponent.getKeymap(
				JTextComponent.DEFAULT_KEYMAP);
		Keymap newKeymap = JTextComponent.addKeymap(null, parentKeymap);
		addSyntaxActionsToKeymap(newKeymap);
		editorPane.setKeymap(newKeymap);

		for (ElianaComponent component : components) {
			component.install(editorPane);
		}
	}

	@Override
	public void deinstall(JEditorPane editorPane) {
		for (ElianaComponent c : components) {
			c.deinstall(editorPane);
		}
	}


	/**
	 * Add keyboard actions to the keymap
	 * @param map
	 */
	private void addSyntaxActionsToKeymap(Keymap map) {
		for (ElianaAction action : actions) {
			map.addActionForKeyStroke(action.getKeyStroke(), action);
		}
	}

	/**
	 * This is called by Swing to create a Document for the JEditorPane document
	 * This may be called before you actually get a reference to the control.
	 * We use it here to create a proper lexer and pass it to the
	 * SyntaxDocument we return.
	 * @return The new Document.
	 */
	@Override
	public Document createDefaultDocument() {
		return new ElianaDocument(lexer);
	}

}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.event.DocumentEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.undo.UndoManager;

import eliana.actions.ActionUtils;
import eliana.actions.IndentAction;
import eliana.actions.UnindentAction;

/**
 * A document that supports being highlighted.  The document maintains an
 * internal List of all the Tokens.  The Tokens are updated using
 * a Lexer, passed to it during construction.
 * 
 * @author Ayman Al-Sairafi
 */
@SuppressWarnings("serial")
public class ElianaDocument extends PlainDocument {

	/**
	 * If <code>true</code>, {@link IndentAction} and {@link UnindentAction}
	 * ignores {@link PlainDocument#tabSizeAttribute}.
	 */
	public static final String INDENT_WITH_TABS_ATTRIBUTE = "indentWithTabs";

	private final ElianaLexer lexer;
	private List<? extends ElianaToken> tokens;
	private final UndoManager undo = new CompoundUndoManager();

	public ElianaDocument(ElianaLexer lexer) {
		putProperty(PlainDocument.tabSizeAttribute, 4);
		putProperty(INDENT_WITH_TABS_ATTRIBUTE, true);

		this.lexer = lexer;
		// Listen for undo and redo events
		addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent evt) {
				if (evt.getEdit().isSignificant()) {
					undo.addEdit(evt.getEdit());
				}
			}
		});
	}



	public boolean getIndentWithTabs() {
		return (Boolean) getProperty(INDENT_WITH_TABS_ATTRIBUTE);
	}

	public void setIndentWithTabs(boolean indentWithtabs) {
		putProperty(INDENT_WITH_TABS_ATTRIBUTE, indentWithtabs);
	}

	public int getTabSize() {
		return (Integer) getProperty(PlainDocument.tabSizeAttribute);
	}

	public void setTabSize(int tabSize) {
		putProperty(PlainDocument.tabSizeAttribute, tabSize);
	}

	public String getSpaceIndentString() {
		String indentChar = getIndentWithTabs() ? "\t" : " ";
		return ActionUtils.repeatString(indentChar, getTabSize());
	}

	/**
	 * Parse the entire document and return list of tokens that do not already
	 * exist in the tokens list.  There may be overlaps, and replacements,
	 * which we will cleanup later.
	 */
	private void parse() {
		// if we have no lexer, then we must have no tokens...
		if (lexer == null) {
			tokens = null;
			return;
		}
		long ts = System.nanoTime();
		int len = getLength();
		try {
			String text = getText(0, getLength());
			tokens = lexer.lexForSyntaxHighlighting(text.toCharArray());
		} catch (BadLocationException ex) {
			log.log(Level.SEVERE, null, ex);
		} finally {
			if (log.isLoggable(Level.FINEST)) {
				log.finest(String.format("Parsed %d in %d ms, giving %d tokens\n",
						len, (System.nanoTime() - ts) / 1000000, tokens.size()));
			}
		}
	}

	@Override
	protected void fireChangedUpdate(DocumentEvent e) {
		parse();
		super.fireChangedUpdate(e);
	}

	@Override
	protected void fireInsertUpdate(DocumentEvent e) {
		parse();
		super.fireInsertUpdate(e);
	}

	@Override
	protected void fireRemoveUpdate(DocumentEvent e) {
		parse();
		super.fireRemoveUpdate(e);
	}

	@Override
	protected void fireUndoableEditUpdate(UndoableEditEvent e) {
		parse();
		super.fireUndoableEditUpdate(e);
	}

	/**
	 * Replace the token with the replacement string
	 * @param token
	 * @param replacement
	 */
	public void replaceToken(ElianaToken token, String replacement) {
		try {
			replace(token.getStart(), token.getLength(), replacement, null);
		} catch (BadLocationException ex) {
			log.log(Level.WARNING, "unable to replace token: " + token, ex);
		}
	}

	/**
	 * This class is used to iterate over tokens between two positions
	 * 
	 */
	class TokenIterator implements ListIterator<ElianaToken> {

		int start;
		int end;
		int index = 0;

		private TokenIterator(int start, int end) {
			this.start = start;
			this.end = end;
			if (tokens != null && !tokens.isEmpty()) {

				ElianaToken token = new DefaultToken(null, start, end - start);

				index = Collections.binarySearch(tokens, token);
				// we will probably not find the exact token...
				if (index < 0) {
					// so, start from one before the token where we should be...
					// -1 to get the location, and another -1 to go back..
					index = (-index - 1 - 1 < 0) ? 0 : (-index - 1 - 1);
					ElianaToken t = tokens.get(index);
					// if the prev token does not overlap, then advance one
					if (t.getEnd() <= start) {
						index++;
					}

				}
			}
		}

		@Override
		public boolean hasNext() {
			if (tokens == null) {
				return false;
			}
			if (index >= tokens.size()) {
				return false;
			}
			ElianaToken t = tokens.get(index);
			if (t.getStart() >= end) {
				return false;
			}
			return true;
		}

		@Override
		public ElianaToken next() {
			return tokens.get(index++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean hasPrevious() {
			if (tokens == null) {
				return false;
			}
			if (index <= 0) {
				return false;
			}
			ElianaToken t = tokens.get(index);
			if (t.getEnd() <= start) {
				return false;
			}
			return true;
		}

		@Override
		public ElianaToken previous() {
			return tokens.get(index--);
		}

		@Override
		public int nextIndex() {
			return index + 1;
		}

		@Override
		public int previousIndex() {
			return index - 1;
		}

		@Override
		public void set(ElianaToken e) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void add(ElianaToken e) {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Return an iterator of tokens between p0 and p1.
	 * @param start start position for getting tokens
	 * @param end position for last token
	 * @return Iterator for tokens that overal with range from start to end
	 */
	public Iterator<ElianaToken> getTokenIterator(int start, int end) {
		return new TokenIterator(start, end);
	}

	public List<ElianaToken> getTokens() {
		return Collections.unmodifiableList(tokens);
	}

	/**
	 * 
	 * @param token
	 * @return The index of token or a negative value if not found
	 */
	public int indexOf(ElianaToken token) {
		return Collections.binarySearch(tokens, token);
	}

	/**
	 * Find the token at a given position.  May return null if no token is
	 * found (whitespace skipped) or if the position is out of range:
	 * @param pos
	 * @return the token or null.
	 */
	public ElianaToken getTokenAt(int pos) {
		if (tokens == null || tokens.isEmpty() || pos > getLength()) {
			return null;
		}
		ElianaToken tok = null;
		ElianaToken tKey = new DefaultToken(null, pos, 1);
		int ndx = Collections.binarySearch(tokens, tKey);
		if (ndx < 0) {
			// so, start from one before the token where we should be...
			// -1 to get the location, and another -1 to go back..
			ndx = (-ndx - 1 - 1 < 0) ? 0 : (-ndx - 1 - 1);
			ElianaToken t = tokens.get(ndx);
			if ((t.getStart() <= pos) && (pos <= t.getEnd())) {
				tok = t;
			}
		} else {
			tok = tokens.get(ndx);
		}
		return tok;
	}

	/**
	 * This is used to return the other part of a paired token in the document.
	 * A paired part has token.pairValue <> 0, and the paired token will
	 * have the negative of t.pairValue.
	 * This method properly handles nestings of same pairValues, but overlaps
	 * are not checked.
	 * if The document does not contain a paired
	 * @param t
	 * @return the other pair's token, or null if nothing is found.
	 */
	public ElianaToken getPairFor(ElianaToken t) {
		int pairValue = t.getPairValue();

		if (t == null || pairValue == 0) {
			return null;
		}
		ElianaToken p = null;
		int ndx = tokens.indexOf(t);

		// w will be similar to a stack. The openners weght is added to it
		// and the closers are subtracted from it (closers are already
		// negative)
		int w = pairValue;
		int direction = (pairValue > 0) ? 1 : -1;
		boolean done = false;
		int v = Math.abs(pairValue);
		while (!done) {
			ndx += direction;
			if (ndx < 0 || ndx >= tokens.size()) {
				break;
			}
			ElianaToken current = tokens.get(ndx);
			if (Math.abs(current.getPairValue()) == v) {
				w += current.getPairValue();
				if (w == 0) {
					p = current;
					done = true;
				}
			}
		}

		return p;
	}

	/**
	 * Perform an undo action, if possible
	 */
	public void doUndo() {
		if (undo.canUndo()) {
			undo.undo();
			parse();
		}
	}

	/**
	 * Perform a redo action, if possible.
	 */
	public void doRedo() {
		if (undo.canRedo()) {
			undo.redo();
			parse();
		}
	}

	/**
	 * Return a matcher that matches the given pattern on the entire document
	 * @param pattern
	 * @return matcher object
	 */
	public Matcher getMatcher(Pattern pattern) {
		return getMatcher(pattern, 0, getLength());
	}

	/**
	 * Return a matcher that matches the given pattern in the part of the
	 * document starting at offset start.  Note that the matcher will have
	 * offset starting from <code>start</code>
	 *
	 * @param pattern
	 * @param start
	 * @return matcher that <b>MUST</b> be offset by start to get the proper
	 * location within the document; or <code>null</code>
	 */
	public Matcher getMatcher(Pattern pattern, int start) {
		return getMatcher(pattern, start, getLength() - start);
	}

	/**
	 * Return a matcher that matches the given pattern in the part of the
	 * document starting at offset start and ending at start + length.
	 * Note that the matcher will have
	 * offset starting from <code>start</code>
	 *
	 * @param pattern
	 * @param start
	 * @param length
	 * @return matcher that <b>MUST</b> be offset by start to get the proper
	 * location within the document; or <code>null</code>
	 */
	public Matcher getMatcher(Pattern pattern, int start, int length) {
		Matcher matcher = null;
		if (getLength() == 0) {
			return null;
		}
		try {
			Segment seg = new Segment();
			getText(start, length, seg);
			matcher = pattern.matcher(seg);
		} catch (BadLocationException ex) {
			log.log(Level.SEVERE, "Requested offset: " + ex.offsetRequested(), ex);
		}
		return matcher;
	}

	/**
	 * This will discard all undoable edits
	 */
	public void clearUndos() {
		undo.discardAllEdits();
	}

	/**
	 * Gets the line at given position.  The line returned will NOT include
	 * the line terminator '\n'
	 * @param pos Position (usually from text.getCaretPosition()
	 * @return the STring of text at given position
	 * @throws BadLocationException
	 */
	public String getLineAt(int pos) throws BadLocationException {
		Element e = getParagraphElement(pos);
		Segment seg = new Segment();
		getText(e.getStartOffset(), e.getEndOffset() - e.getStartOffset(), seg);
		char last = seg.last();
		if (last == '\n' || last == '\r') {
			return seg.subSequence(0, seg.length() - 1).toString();
		}
		return seg.toString();
	}

	/**
	 * Deletes the line at given position
	 * @param pos
	 * @throws javax.swing.text.BadLocationException
	 */
	public void removeLineAt(int pos)
			throws BadLocationException {
		Element e = getParagraphElement(pos);
		remove(e.getStartOffset(), getElementLength(e));
	}

	/**
	 * Replace the line at given position with the given string, which can span
	 * multiple lines
	 * @param pos
	 * @param newLines
	 * @throws javax.swing.text.BadLocationException
	 */
	public void replaceLineAt(int pos, String newLines)
			throws BadLocationException {
		Element e = getParagraphElement(pos);
		replace(e.getStartOffset(), getElementLength(e), newLines, null);
	}

	/**
	 * Helper method to get the length of an element and avoid getting
	 * a too long element at the end of the document
	 * @param e
	 * @return the length of e
	 */
	private int getElementLength(Element e) {
		int end = e.getEndOffset();
		if (end >= (getLength() - 1)) {
			end--;
		}
		return end - e.getStartOffset();
	}

	//	/**
	//	 * Gets the text without the comments. For example for the string
	//	 * <code>{ // it's a comment</code> this method will return "{ ".
	//	 * @param aStart start of the text.
	//	 * @param anEnd end of the text.
	//	 * @return String for the line without comments (if exists).
	//	 */
	//	public synchronized String getUncommentedText(int aStart, int anEnd) {
	//		readLock();
	//		StringBuilder result = new StringBuilder();
	//		Iterator<Token> iter = getTokens(aStart, anEnd);
	//		while (iter.hasNext()) {
	//			Token t = iter.next();
	//			TokenType tokenType = t.getType();
	//			if (! tokenType.isComment()) {
	//				result.append(t.getText(this));
	//			}
	//		}
	//		readUnlock();
	//		return result.toString();
	//	}

	/**
	 * Returns the starting position of the line at pos
	 * @param pos
	 * @return starting position of the line
	 */
	public int getLineStartOffset(int pos) {
		return getParagraphElement(pos).getStartOffset();
	}

	/**
	 * Returns the end position of the line at pos.
	 * Does a bounds check to ensure the returned value does not exceed
	 * document length
	 * @param pos
	 * @return the end position of the line at pos.
	 */
	public int getLineEndOffset(int pos) {
		int end = 0;
		end = getParagraphElement(pos).getEndOffset();
		if (end >= getLength()) {
			end = getLength();
		}
		return end;
	}

	/**
	 * @return the number of lines in this document
	 */
	public int getLineCount() {
		Element e = getDefaultRootElement();
		int cnt = e.getElementCount();
		return cnt;
	}

	/**
	 * The line numbers are zero based.
	 * @param pos
	 * @return the line number at given position.
	 */
	public int getLineNumberAt(int pos) {
		int lineNr = getDefaultRootElement().getElementIndex(pos);
		return lineNr;
	}

	@Override
	public String toString() {
		return "SyntaxDocument(" + lexer + ", " +
				((tokens == null) ? 0 : tokens.size()) + " tokens)" +
				"@" + hashCode();
	}

	private static final Logger log = Logger.getLogger(
			ElianaDocument.class.getName());
}

/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;




public class DefaultToken implements ElianaToken {

	public final ElianaTokenType type;
	public final int start;
	public final int length;

	public final byte pairValue;



	/**
	 * Constructs a new token
	 * @param type
	 * @param start
	 * @param length
	 */
	public DefaultToken(ElianaTokenType type, int start, int length) {
		this.type = type;
		this.start = start;
		this.length = length;
		pairValue = 0;
	}

	/**
	 * Construct a new part of pair token
	 * @param type
	 * @param start
	 * @param length
	 * @param pairValue
	 */
	public DefaultToken(ElianaTokenType type, int start, int length,
			byte pairValue) {
		this.type = type;
		this.start = start;
		this.length = length;
		this.pairValue = pairValue;
	}



	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DefaultToken) {
			DefaultToken token = (DefaultToken) obj;
			return (start == token.start) &&
					(length == token.length) &&
					(type.equals(token.type));
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return start;
	}

	@Override
	public String toString() {
		return String.format(
				"Token{type=%s; start=%d; length=%d; pairValue=%d}",
				type, start, length, pairValue);
	}

	@Override
	public int compareTo(ElianaToken other) {
		if (start !=  other.getStart()) {
			return (start - other.getStart());
		} else if(length != other.getLength()) {
			return (length - other.getLength());
		} else {
			return 0;
		}
	}

	@Override
	public int getEnd() {
		return start + length;
	}

	@Override
	public ElianaTokenType getType() {
		return type;
	}

	@Override
	public int getStart() {
		return start;
	}

	@Override
	public int getLength() {
		return length;
	}

	@Override
	public int getPairValue() {
		return pairValue;
	}
}

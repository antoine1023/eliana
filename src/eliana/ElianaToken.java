/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;




/**
 * Represents a simple token.
 * 
 * @see ElianaToken
 */
public interface ElianaToken extends Comparable<ElianaToken> {

	/**
	 * @return The type of the {@link ElianaToken}.
	 */
	public ElianaTokenType getType();

	/**
	 * @return the start offset
	 */
	public int getStart();

	/**
	 * @return the length
	 */
	public int getLength();

	/**
	 * @return The end position of the token, start + length
	 */
	public int getEnd();

	/**
	 * The pair value to use if this token is one of a pair.
	 * 
	 * This is how it is used:
	 * The openning part will have a positive number X
	 * The closing part will have a negative number X
	 * 
	 * X should be unique for a pair:
	 * 
	 * <pre>
	 *    for [ pairValue = +1
	 *    for ] pairValue = -1
	 * <pre>
	 * 
	 * @return the pair value
	 */
	public int getPairValue();

}

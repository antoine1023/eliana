/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.text.Segment;
import javax.swing.text.TabExpander;
import javax.swing.text.Utilities;

public final class TextStyle {

	public static final TextStyle DEFAULT_STYLE = new TextStyle(
			Color.BLACK, Font.PLAIN);

	/**
	 * Works only if the background color is not <code>null</code>.
	 */
	public static final int UNDERLINED = 0x8, FRAMED = 0x10;

	private Color foreground;

	/**
	 * The background or <code>null</code> if transparent.
	 */
	private Color background;

	private int fontStyle;



	public TextStyle(Color foreground, int fontStyle) {
		this(foreground, null, fontStyle);
	}

	/**
	 * 
	 * @param foreground the foreground
	 * @param background the background or <code>null</code>
	 * @param fontStyle
	 */
	public TextStyle(Color foreground, Color background, int fontStyle) {
		this.foreground = foreground;
		this.background = background;
		this.fontStyle = fontStyle;
	}

	public boolean isBold() {
		return (fontStyle & Font.BOLD) != 0;
	}

	public void setBold(boolean bold) {
		if (bold) {
			fontStyle |= Font.BOLD;
		} else {
			int mask = -1 ^ Font.BOLD;
			fontStyle = (fontStyle & (mask));
		}
	}

	public boolean isItalic() {
		return (fontStyle & Font.ITALIC) != 0;
	}

	public void setItalic(boolean italic) {
		if (italic) {
			fontStyle |= Font.ITALIC;
		} else {
			fontStyle = (fontStyle & (-1 ^ Font.ITALIC));
		}
	}



	public int getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(int fontStyle) {
		this.fontStyle = fontStyle;
	}



	public Color getBackground() {
		return background;
	}

	public void setBackground(Color background) {
		this.background = background;
	}



	public Color getForeground() {
		return foreground;
	}

	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}



	/**
	 * Draw text.  This can directly call the Utilities.drawTabbedText.
	 * 
	 * Sub-classes can override this method to provide any other decorations.
	 * @param  segment - the source of the text
	 * @param  x - the X origin >= 0
	 * @param  y - the Y origin >= 0
	 * @param  graphics - the graphics context
	 * @param tabExpander - how to expand the tabs. If this value is
	 * <code>null</code>, tabs will be expanded as a space character.
	 * @param startOffset - starting offset of the text in the document >= 0
	 * 
	 * @return the X location at the end of the rendered text
	 */
	public int drawText(Segment segment, int x, int y,
			Graphics graphics, TabExpander tabExpander, int startOffset) {

		graphics.setFont(graphics.getFont().deriveFont(fontStyle));

		if (background != null) {

			FontMetrics fontMetrics = graphics.getFontMetrics();

			int ascent = fontMetrics.getAscent();
			int height = ascent + fontMetrics.getDescent();
			int width = Utilities.getTabbedTextWidth(
					segment, fontMetrics, 0, tabExpander, startOffset);

			int rX = x;
			int rY = y - ascent;
			int rWidth = width;
			int rHeight = height - 1;

			graphics.setColor(background);
			if ((fontStyle & FRAMED) != 0) {
				graphics.drawRect(rX, rY, rWidth, rHeight);
			} else if ((fontStyle & UNDERLINED) != 0) {
				graphics.drawLine(rX, y + 2, rX + rWidth, y + 2);
			} else {
				graphics.fillRect(rX, rY, rWidth, rHeight);
			}
		}

		graphics.setColor(foreground);
		x = Utilities.drawTabbedText(segment, x, y, graphics, tabExpander,
				startOffset);

		return x;
	}
}

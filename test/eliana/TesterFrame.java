/*
 * Copyright 2008 Ayman Al-Sairafi ayman.alsairafi@gmail.com
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eliana;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import eliana.actions.CaretMonitor;

@SuppressWarnings("serial")
public class TesterFrame extends JFrame {

	private final MyEditorKit mySyntaxKit;

	private JEditorPane editorPane;
	private JScrollPane scrollPane;
	private JLabel lblCaretPos;
	private JLabel lblToken;

	private static final Logger LOGGER = Logger.getLogger(
			TesterFrame.class.getName());


	/** Creates new form Tester */
	public TesterFrame() {

		mySyntaxKit = new MyEditorKit(this);

		initComponents();

		new CaretMonitor(editorPane, lblCaretPos);

		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}



	private void initComponents() {

		editorPane = new JEditorPane();
		scrollPane = new JScrollPane(editorPane);

		setTitle("JSyntaxPane Tester");

		lblCaretPos = new JLabel("Caret Position");
		lblCaretPos.setHorizontalAlignment(SwingConstants.RIGHT);

		editorPane.setEditorKit(mySyntaxKit);
		editorPane.setFont(new Font(Font.MONOSPACED, 0, 14));
		editorPane.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent evt) {
				jEdtTestCaretUpdate(evt);
			}
		});

		lblToken = new JLabel("Token under cursor");

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
								.addComponent(lblToken, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
								.addGroup(layout.createSequentialGroup()
										.addGap(262, 262, 262)
										.addComponent(lblCaretPos, 10, 119, GroupLayout.PREFERRED_SIZE))
										.addComponent(scrollPane, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE))
										.addContainerGap())
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(scrollPane, 10, 395, Short.MAX_VALUE)
						.addGap(2, 2, 2)
						.addComponent(lblToken, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(lblCaretPos, GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
								.addContainerGap())
				);

		pack();
	}

	private void jEdtTestCaretUpdate(CaretEvent evt) {
		if (editorPane.getDocument() instanceof ElianaDocument) {
			ElianaDocument sDoc = (ElianaDocument) editorPane.getDocument();
			String infos = "?";
			ElianaToken token = sDoc.getTokenAt(evt.getDot());
			if (token != null) {
				try {
					String text = sDoc.getText(token.getStart(),
							Math.min(token.getLength(), 40));
					infos = token.toString() + ": " + text;
				} catch (BadLocationException ex) {
					LOGGER.severe("BadLocationException with " + token);
				}
			}
			if (infos.length() > 100) {
				infos += "...";
			}
			lblToken.setText(infos);
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TesterFrame().setVisible(true);
			}
		});
	}

}

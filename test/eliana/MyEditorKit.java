/*
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eliana;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;

import eliana.actions.DefaultFindView;
import eliana.actions.DeleteLinesAction;
import eliana.actions.FindNextAction;
import eliana.actions.Finder;
import eliana.actions.GoToLineAction;
import eliana.actions.IndentAction;
import eliana.actions.RedoAction;
import eliana.actions.ShowFindReplaceDialogAction;
import eliana.actions.UndoAction;
import eliana.actions.UnindentAction;
import eliana.components.LineNumbersRuler;
import eliana.components.PairsMarker;
import eliana.components.TokenMarker;


@SuppressWarnings("serial")
public class MyEditorKit extends ElianaEditorKit {

	private final DefaultFindView findView;

	/////////////////// Actions

	public final DeleteLinesAction deleteLinesAction;
	public final FindNextAction findNextAction;
	public final ShowFindReplaceDialogAction findReplaceAction;
	public final GoToLineAction goToLineAction;
	public final IndentAction indentAction;
	public final RedoAction redoAction;
	public final UndoAction undoAction;
	public final UnindentAction unindentAction;

	/////////////////// Components

	private final LineNumbersRuler lineNumbersRuler;
	private final PairsMarker pairsMarker;
	private final TokenMarker tokenMarker;



	public MyEditorKit(JFrame parent) {
		super(new MyLexer(), createTheme());

		Finder finder = new Finder();


		{ // Actions

			deleteLinesAction = new DeleteLinesAction();
			addAction(deleteLinesAction);

			findReplaceAction = new ShowFindReplaceDialogAction(finder);
			addAction(findReplaceAction);

			findNextAction = new FindNextAction(finder);
			addAction(findNextAction);

			goToLineAction = new GoToLineAction();
			addAction(goToLineAction);

			indentAction = new IndentAction();
			addAction(indentAction);

			redoAction = new RedoAction();
			addAction(redoAction);

			undoAction = new UndoAction();
			addAction(undoAction);

			unindentAction = new UnindentAction();
			addAction(unindentAction);

		}

		{ // Components

			findView = new DefaultFindView(parent,
					findNextAction, findNextAction, findNextAction);
			finder.setView(findView);


			lineNumbersRuler = new LineNumbersRuler();
			lineNumbersRuler.setGoToLineAction(goToLineAction);
			addComponent(lineNumbersRuler);

			pairsMarker = new PairsMarker();
			addComponent(pairsMarker);

			tokenMarker = new TokenMarker();
			addComponent(tokenMarker);

		}
	}


	private static Theme createTheme() {

		Theme styles = new Theme();

		styles.put(DefaultTokenType.COMMENT,
				new TextStyle(Color.GREEN, Font.ITALIC));

		styles.put(DefaultTokenType.NUMBER,
				new TextStyle(Color.BLACK, Color.RED,
						Font.BOLD | TextStyle.FRAMED));

		styles.put(DefaultTokenType.IDENTIFIER,
				new TextStyle(Color.BLACK, Color.RED, TextStyle.UNDERLINED));

		return styles;
	}
}

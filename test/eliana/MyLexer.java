/*
 * Copyright 2013 antoine1023 antoine1023.dev@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License
 *       at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eliana;

import java.util.ArrayList;
import java.util.List;

import eliana.DefaultToken;
import eliana.DefaultTokenType;
import eliana.ElianaLexer;
import eliana.ElianaToken;


/**
 * 
 * This is a tiny and ugly lexer, written to test.
 */
public class MyLexer implements ElianaLexer {

	private StringBuilder currentString;
	private int index;
	private List<ElianaToken> tokens;

	@Override
	public List<ElianaToken> lexForSyntaxHighlighting(char[] source) {

		currentString = new StringBuilder();
		index = 0;
		tokens = new ArrayList<ElianaToken>();

		for (index = 0; index < source.length; index++) {
			
			char c = source[index];
			
			if (c >= '0' && c <= '9') {
				createStringToken();
				DefaultToken token = new DefaultToken(
						DefaultTokenType.NUMBER, index, 1);
				tokens.add(token);
				
			} else if (c == '(') {
				createStringToken();
				DefaultToken token = new DefaultToken(
						DefaultTokenType.OPERATOR, index, 1, (byte) 1);
				tokens.add(token);
				
			} else if (c == ')') {
				createStringToken();
				DefaultToken token = new DefaultToken(
						DefaultTokenType.OPERATOR, index, 1, (byte) -1);
				tokens.add(token);
				
			} else if (c == ' ') {
				createStringToken();
				
			} else {
				currentString.append(c);
			}
		}
		createStringToken();
		return tokens;
	}

	private void createStringToken() {
		if (currentString.length() == 0) {
			return;
		}
		int start = index - currentString.length();
		DefaultToken token = new DefaultToken(DefaultTokenType.IDENTIFIER,
				start, currentString.length());
		tokens.add(token);
		currentString = new StringBuilder();
	}

}

# Eliana

Eliana is a syntax highlighter for Swing, forked from
<https://code.google.com/p/jsyntaxpane/> and very simplified.

It contains no lexer, you should use your own with it.
